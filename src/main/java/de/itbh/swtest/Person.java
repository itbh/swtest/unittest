package de.itbh.swtest;

public class Person {

	private Gender gender;
	private int age;
	private boolean isStudent;

	public Person(Gender gender, int age, boolean isStudent) {
		this.gender = gender;
		this.age = age;
		this.isStudent = isStudent;
	}

	public Gender getGender() {
		return gender;
	}

	public int getAge() {
		return age;
	}

	public boolean isStudent() {
		return isStudent;
	}

}

# Versicherungsberechnung

Anforderungen an die Versicherungsberechnung:
   
	Calculates rate for students insurance
	Students from 18 years to 26 years pay
	140 Euro when female and 15% more when male
	If no insurance is possible, -1 is returned 


Ausführung der Tests inklusive PIT Tests:

    mvn clean test
	
Das Ergebnis der Pit Tests ist in target/pit-reports

Enthalten ist auch die Codeanalyse über SonarQube.
Dazu entweder lokal oder via Docker einen Sonarqube starten:

	docker run -d --name sonarqube -p 9000:9000 sonarqube
	
Ausführung der Analyse mit

	mvn clean test sonar:sonar
	
Ein Lösungsvorschlag findet sich im Branch 

	daniel.loesungsvorschlag

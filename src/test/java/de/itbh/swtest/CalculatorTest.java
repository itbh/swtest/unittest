package de.itbh.swtest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

class CalculatorTest {

	@Test
	void testClassExists() {
		// arrange
		Calculator c = new Calculator();
				
		// act 
		
		// assert
		assertNotNull(c);
	}
	
	@Test
	void testMaleStudent17() {
		// arrange
		Calculator c = new Calculator();
		Person p = new Person(Gender.MALE, 17, true);
		
		// act
		double result = c.calculate(p);
				
		// assert
		assertEquals(-1, result);
	}
}

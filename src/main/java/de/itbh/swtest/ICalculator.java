package de.itbh.swtest;

public interface ICalculator {

	/**
	 * 
	 * Calculates rate for students insurance. Students from 18 years to 26 years
	 * pay 140 Euro when female and 15% more when male. If no insurance is possible,
	 * -1 is returned.
	 * 
	 * @param person Person to calculate insurance
	 * @return -1 or rate
	 */
	public double calculate(Person person);

}